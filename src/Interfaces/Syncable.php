<?php

namespace RoobieBoobieee\Gitlab\Interfaces;

interface Syncable
{
    public static function primaryKey();
}
