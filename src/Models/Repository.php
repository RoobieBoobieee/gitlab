<?php

namespace RoobieBoobieee\Gitlab\Models;

use Illuminate\Database\Eloquent\Model;
use RoobieBoobieee\Gitlab\Interfaces\Syncable;

class Repository extends Model implements Syncable
{
    protected $table = 'gitlab_repositories';

    protected $fillable = [
        'id',
        'description',
        'name',
        'name_with_namespace',
        'path',
        'path_with_namespace',
        'gl_created_at',
        'default_branch',
        'star_count',
        'forks_count',
        'namespace_id',
        'gl_last_activity_at',
        'empty_repo',
        'archived',
        'visibility',
        'owner_id',
        'resolve_outdated_diff_discussions',
        'container_registry_enabled',
        'issues_enabled',
        'merge_requests_enabled',
        'wiki_enabled',
        'jobs_enabled',
        'snippets_enabled',
        'issues_access_level',
        'repository_access_level',
        'merge_requests_access_level',
        'wiki_access_level',
        'builds_access_level',
        'snippets_access_level',
        'shared_runners_enabled',
        'lfs_enabled',
        'creator_id',
        'import_status',
        'open_issues_count',
        'auto_cancel_pending_pipelines',
        'merge_method',
        'auto_devops_deploy_strategy',
        'external_authorization_classification_label',
        'only_allow_merge_if_pipeline_succeeds',
        'request_access_enabled',
        'only_allow_merge_if_all_discussions_are_resolved',
        'printing_merge_request_link_enabled',
        'auto_devops_enabled',
        'mirror',
    ];

    public static function primaryKey()
    {
        return 'id';
    }

    public function setUserName($username)
    {
        return $this->username = $username;
    }
}
