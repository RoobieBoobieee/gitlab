<?php

namespace RoobieBoobieee\Gitlab\Models;

use Illuminate\Database\Eloquent\Model;
use RoobieBoobieee\Gitlab\Interfaces\Syncable;

class Commit extends Model implements Syncable
{
    protected $table = 'gitlab_commits';

    protected $fillable = [
        'id',
        'repo_id',
        'short_id',
        'gl_created_at',
        'title',
        'description',
        'gl_authored_date',
        'author_email',
        'gl_committed_date',
        'committer_email',
    ];

    public static function primaryKey()
    {
        return 'id';
    }

    public function repository()
    {
        return $this->hasOne(Repository::class, 'id', 'repo_id');
    }

    public function saveRepository($id)
    {
        $this->repo_id = $page;

        return $this->save();
    }

    public function saveLastPage($page)
    {
        $this->repository->last_commits_page = $page;

        return $this->repository->save();
    }
}
