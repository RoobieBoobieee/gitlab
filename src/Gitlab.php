<?php

namespace RoobieBoobieee\Gitlab;

use GuzzleHttp\Client;
use RoobieBoobieee\Gitlab\Models\Commit;

class Gitlab
{
    private $username;

    private $password;

    private $user;

    private $emails;

    private $client;

    public function __construct($username, $password)
    {
        $this->client = new Client([
            'base_uri' => 'https://gitlab.com/api/v4/',
        ]);
        $this->username = $username;
        $this->password = $password;

        $this->user   = $this->call('user');
        $this->emails = $this->call('user/emails');
    }

    public function call($endpoint, $options = [])
    {
        $options = array_merge_recursive([
            'headers' => ['PRIVATE-TOKEN' => $this->password],
        ], $options);

        $response = $this->client->request('get', $endpoint, $options);

        return json_decode($response->getBody());
    }

    public function username()
    {
        return $this->user->username;
    }

    public function id()
    {
        return $this->user->id;
    }

    public function commits()
    {
        $emails = array_column($this->emails, 'email');
        array_push($emails, $this->user->email);

        return Commit::whereIn('author_email', $emails);
    }
}
