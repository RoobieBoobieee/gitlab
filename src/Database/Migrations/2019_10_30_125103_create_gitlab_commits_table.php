<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGitlabCommitsTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('gitlab_commits', function (Blueprint $table) {
            $table->string('id');
            $table->string('short_id');
            $table->datetime('gl_created_at');
            $table->string('title');
            $table->longtext('description')->nullable();
            $table->datetime('gl_authored_date');
            $table->string('author_email');
            $table->datetime('gl_committed_date');
            $table->string('committer_email');
            $table->unsignedBigInteger('repo_id');
            $table->string('checksum', 32);
            $table->timestamps();

            $table->unique('id');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('gitlab_commits');
    }
}
