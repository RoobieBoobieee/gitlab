<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGitlabRepositoriesTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('gitlab_repositories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->longtext('description')->nullable();
            $table->string('name');
            $table->string('name_with_namespace');
            $table->string('path');
            $table->string('path_with_namespace');
            $table->datetime('gl_created_at');
            $table->string('default_branch');
            $table->integer('star_count');
            $table->integer('forks_count');
            $table->unsignedBigInteger('namespace_id');
            $table->datetime('gl_last_activity_at');
            $table->boolean('empty_repo')->default(false);
            $table->boolean('archived');
            $table->string('visibility');
            $table->unsignedBigInteger('owner_id');
            $table->boolean('resolve_outdated_diff_discussions')->nullable();
            $table->boolean('container_registry_enabled');
            $table->boolean('issues_enabled');
            $table->boolean('merge_requests_enabled');
            $table->boolean('wiki_enabled');
            $table->boolean('jobs_enabled');
            $table->boolean('snippets_enabled');
            $table->string('issues_access_level');
            $table->string('repository_access_level');
            $table->string('merge_requests_access_level');
            $table->string('wiki_access_level');
            $table->string('builds_access_level');
            $table->string('snippets_access_level');
            $table->string('shared_runners_enabled');
            $table->string('lfs_enabled');
            $table->unsignedBigInteger('creator_id');
            $table->string('import_status');
            $table->integer('open_issues_count');
            $table->string('auto_cancel_pending_pipelines');
            $table->string('merge_method');
            $table->string('auto_devops_deploy_strategy');
            $table->string('external_authorization_classification_label');
            $table->boolean('only_allow_merge_if_pipeline_succeeds');
            $table->boolean('request_access_enabled');
            $table->boolean('only_allow_merge_if_all_discussions_are_resolved')->nullable();
            $table->boolean('printing_merge_request_link_enabled');
            $table->boolean('auto_devops_enabled');
            $table->boolean('mirror')->default(false);
            $table->string('checksum', 32);
            $table->boolean('sync')->default(false);
            $table->string('username')->nullable()->default(null);
            $table->integer('last_commits_page')->nullable()->default(1);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('gitlab_repositories');
    }
}
