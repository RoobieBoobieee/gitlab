<?php

namespace RoobieBoobieee\Gitlab\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use RoobieBoobieee\Gitlab\Models\Commit;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use RoobieBoobieee\Gitlab\Models\Repository;

class SyncGitlab implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $username;

    private $password;

    /**
     * Create a new job instance.
     */
    public function __construct($username, $password)
    {
        $this->username = $username;
        $this->password = $password;
    }

    /**
     * Execute the job.
     */
    public function handle()
    {
        SyncData::dispatch($this->username, $this->password, Repository::class, 'users/' . $this->username . '/projects');

        $repos = Repository::where('sync', true)->where('username', $this->username)->get();
        foreach ($repos as $repo) {
            SyncData::dispatch($this->username, $this->password, Commit::class, 'projects/' . $repo->id . '/repository/commits', ['repo_id' => $repo->id]);
        }
    }
}
